package hr.ferit.bernardkekelic.gdjesam.model

import android.os.Environment
import java.io.File

class CurrentImage(
    var name: String = "Nepoznata adresa",
    var imageFile: File = File(Environment.getExternalStorageDirectory(), "")

)