package hr.ferit.bernardkekelic.gdjesam.model

class CurrentLocation(
    var latitude: Double = 0.00,
    var longitude: Double = 0.00,
    var state: String = "No state",
    var place: String = "No place",
    var address: String = "No address"
)