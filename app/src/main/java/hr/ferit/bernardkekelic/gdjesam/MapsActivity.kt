package hr.ferit.bernardkekelic.gdjesam

import android.Manifest
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import hr.ferit.bernardkekelic.gdjesam.permissions.hasPermissionCompat
import hr.ferit.bernardkekelic.gdjesam.permissions.requestPermisionCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.media.AudioManager
import android.media.MediaScannerConnection
import android.media.SoundPool
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import com.google.android.gms.maps.model.BitmapDescriptor
import hr.ferit.bernardkekelic.gdjesam.model.CurrentImage
import hr.ferit.bernardkekelic.gdjesam.model.CurrentLocation
import java.io.File
import java.text.SimpleDateFormat


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    //Google Maps
    private lateinit var map: GoogleMap

    //Location
    private val locationPermission = Manifest.permission.ACCESS_FINE_LOCATION
    private val locationRequestCode = 10
    private lateinit var locationManager: LocationManager
    private var currentLocation: CurrentLocation = CurrentLocation()

    //Sound
    private lateinit var mSoundPool: SoundPool
    private var mLoaded: Boolean = false
    var mSoundMap: HashMap<Int, Int> = HashMap()

    //Camera & Notification
    val REQUEST_IMAGE_CAPTURE = 1
    var currentImage = CurrentImage()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpUi()
        trackLocation()
    }

    override fun onResume() {
        super.onResume()
        trackLocation()
    }

    private fun setUpUi(){
        setUpMaps()
        setUpLocation()
        loadSounds()

        addMarkerAction.setOnClickListener { addCurrentLocationToMapWithBigMarker()}
        takeAPhotoAction.setOnClickListener { openCameraIntent()}
    }

    //Notification
    private fun showNotification() {
        val onClickNotifyIntent = Intent(Intent.ACTION_VIEW, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        val pendingIntent = PendingIntent.getActivity(this@MapsActivity, 1, onClickNotifyIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val notification = Notification.Builder(this@MapsActivity)
            .setTicker("")
            .setContentTitle(getString(R.string.Notification_text))
            .setContentText(currentImage.name)
            .setSmallIcon(R.drawable.notification_icon_background)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent).notification

        notification.flags = Notification.FLAG_AUTO_CANCEL

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notification)

    }

    //Camera
    private fun openCameraIntent() {

        val sdf = SimpleDateFormat(getString(R.string.simpleDateFormat_patern))
        currentImage.name = currentLocation.address + "_" + sdf.format(Date()) + getString(R.string.image_extension)

        val imageIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)

        currentImage.imageFile = File(Environment.getExternalStorageDirectory(), currentImage.name)
        val uriSavedImage = Uri.fromFile(currentImage.imageFile)

        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage)
        startActivityForResult(imageIntent, REQUEST_IMAGE_CAPTURE)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            showNotification()
            scanner(currentImage.imageFile.getAbsolutePath())
            addCurrentLocationToMapWithCustomMarker(R.drawable.customcamera)
            Toast.makeText(this, getString(R.string.pictureSavedSuccess_Text), Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(this, getString(R.string.pictureSavedError_Text), Toast.LENGTH_SHORT).show()
        }
    }
    private fun scanner(path: String) {

        MediaScannerConnection.scanFile(
            this@MapsActivity,
            arrayOf(path), null
        ) { path, uri -> Log.i("TAG", "Finished scanning $path") }
    }

    //Google Maps
    private fun setUpMaps() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.mapType = GoogleMap.MAP_TYPE_TERRAIN
        map.uiSettings.isZoomControlsEnabled = true
    }


    //Location
    private fun setUpLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }
    private val locationListener = object: LocationListener {
        override fun onProviderEnabled(provider: String?) { }
        override fun onProviderDisabled(provider: String?) { }
        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) { }
        override fun onLocationChanged(location: Location?) {
            updateLocationDisplay(location)
        }
    }
    private fun updateLocationDisplay(location: Location?) {

        val geocoder = Geocoder(this, Locale.getDefault())
        val addresses = geocoder.getFromLocation(location!!.latitude, location!!.longitude,1)

        currentLocation.latitude = location?.latitude
        currentLocation.longitude = location?.longitude
        currentLocation.state = addresses[0].countryName
        currentLocation.place = addresses[0].locality
        currentLocation.address = addresses[0].getAddressLine(0)

        Log.d("TAG", currentLocation.latitude.toString() + currentLocation.longitude.toString())
        view_sirina.text = getString(R.string.geografska_sirina_text) + currentLocation.latitude.toString()
        view_duzina.text = getString(R.string.geografska_duzina_text) + currentLocation.longitude.toString()
        view_drzava.text = getString(R.string.drazava_text) + currentLocation.state.toString()
        view_mjesto.text = getString(R.string.mjesto_text) + currentLocation.place.toString()
        view_adresa.text = getString(R.string.adresa_text) +currentLocation.address.toString()

        addCurrentLocationToMapWithCustomMarker(R.drawable.custommarker)
    }

    private fun addCurrentLocationToMapWithBigMarker(){

        if(currentLocation.latitude!= 0.00 && currentLocation.longitude!= 0.00){
            val markerColor = BitmapDescriptorFactory.HUE_BLUE
            val currentLocation = LatLng(currentLocation.latitude, currentLocation.longitude)

            map.addMarker(MarkerOptions()
                .position(currentLocation)
                .icon(BitmapDescriptorFactory.defaultMarker(markerColor))
            )
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15F))

            playSound(R.raw.clicksound)
            Toast.makeText(this, getString(R.string.locationSaved_text), Toast.LENGTH_SHORT).show()

        }else{
            Toast.makeText(this, getString(R.string.notFoundLocationYet_Text), Toast.LENGTH_SHORT).show()
        }

    }
    private fun addCurrentLocationToMapWithCustomMarker(marker: Int){
        val currentLocation = LatLng(currentLocation.latitude, currentLocation.longitude)
        map.addMarker(MarkerOptions()
            .position(currentLocation)
            .icon(bitmapDescriptorFromVector(this, marker))
        )
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15F))
    }
    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap =
            Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }
    private fun trackLocation() {
        if(hasPermissionCompat(locationPermission)){
            startTrackingLocation()
        } else {
            requestPermisionCompat(arrayOf(locationPermission), locationRequestCode)
        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray) {
        when(requestCode){
            locationRequestCode -> {
                if(grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    trackLocation()
                else
                    Toast.makeText(this, R.string.permissionNotGranted, Toast.LENGTH_SHORT).show()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
    private fun startTrackingLocation() {
        Log.d("TAG", "Tracking location")
        val criteria: Criteria = Criteria()
        criteria.accuracy = Criteria.ACCURACY_FINE
        val provider = locationManager.getBestProvider(criteria, true)
        val minTime = 1000L
        val minDistance = 10.0F
        try{
            locationManager.requestLocationUpdates(provider, minTime, minDistance, locationListener)
        } catch (e: SecurityException){
            Toast.makeText(this, R.string.permissionNotGranted, Toast.LENGTH_SHORT).show()
        }
    }
    override fun onPause() {
        super.onPause()
        locationManager.removeUpdates(locationListener)
    }


    //Sound
    private fun loadSounds() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.mSoundPool = SoundPool.Builder().setMaxStreams(10).build()
        } else {
            this.mSoundPool = SoundPool(10, AudioManager.STREAM_MUSIC, 0)
        }
        this.mSoundPool.setOnLoadCompleteListener { _, _, _ -> mLoaded = true }
        this.mSoundMap[R.raw.clicksound] = this.mSoundPool.load(this, R.raw.clicksound, 1)
    }
    fun playSound(selectedSound: Int) {
        val soundID = this.mSoundMap[selectedSound] ?: 0
        this.mSoundPool.play(soundID, 1f, 1f, 1, 0, 1f)
    }

}
